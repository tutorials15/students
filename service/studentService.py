from respository import studentRepository


def getStudents(request):
    return studentRepository.getOneStudent(request["studentid"])


def addStudent(request):
    studentid = studentRepository.addOneStudent(request)
    return {"id": studentid}


def updateStudent(request):
    return studentRepository.updateOneStudent(request["studentid"])


def deleteStudent(request):
    studentRepository.deleteOneStudent(request["studentid"])
    return request["studentid"]
