from pymongo import MongoClient
from bson.objectid import ObjectId

from respository import studentMapper

mongo_client = MongoClient(host='52.215.219.23:27017', port=27017, username='admin', password='qwerty123')
db = mongo_client["students_db"]
students = db["students"]


def getOneStudent(studentid):
    studententity = students.find_one({"_id": ObjectId(studentid)})
    return studentMapper.mapToStudent(studententity)


def addOneStudent(student):
    objectId = students.insert_one(student).inserted_id
    return str(objectId)


def updateOneStudent(studentid, student):
    objectId = students.update_one({"_id": ObjectId(studentid)}, student).upserted_id
    return str(objectId)


def deleteOneStudent(studentid):
    students.delete_one({"_id": ObjectId(studentid)})
