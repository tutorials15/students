

def mapToStudent(studententity):
    return {
        "id": str(studententity["_id"]),
        "name": studententity["name"],
        "surname": studententity["surname"],
        "grade": studententity["grade"]
    }