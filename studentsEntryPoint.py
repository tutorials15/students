from service import studentService
from mapper import requestMapper
import json


def studentsEntryPoint(event, context):
    request = getRequest(event)
    response = getRequestHandler(event, request)
    return {
        'isBase64Encoded': False,
        'statusCode': 200,
        'body': json.dumps(response)
    }


mappers = {"GET": requestMapper.mapGetRequest,
           "POST": requestMapper.mapPostRequest,
           "PUT": requestMapper.mapPostRequest,
           "DELETE": requestMapper.mapDeleteRequest
           }

handlers = {"GET": studentService.getStudents,
            "POST": studentService.addStudent,
            "PUT": studentService.updateStudent,
            "DELETE": studentService.deleteStudent
            }


def getRequest(event):
    return mappers[event["httpMethod"]](event)


def getRequestHandler(event, request):
    return handlers[event["httpMethod"]](request)
