import json


def mapGetRequest(event):
    getrequest = {
        "studentid": event["pathParameters"]["studentid"]
    }
    if event["queryStringParameters"] is not None:
        if "name" in event["queryStringParameters"]:
            getrequest["name"] = event["queryStringParameters"]["name"]
        if "surname" in event["queryStringParameters"]:
            getrequest["surname"] = event["queryStringParameters"]["surname"]
    return getrequest


def mapPostRequest(event):
    postrequest = json.loads(event["body"])
    return postrequest


def mapDeleteRequest(event):
    deleterequest = {
        "studentid": event["pathParameters"]["studentid"]
    }
    return deleterequest
